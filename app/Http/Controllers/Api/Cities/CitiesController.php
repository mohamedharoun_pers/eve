<?php namespace App\Http\Controllers\Api\Cities;

use App\Helpers\CoordinatesHelper;
use App\Http\Controllers\Controller;
use App\Http\Resources\CitiesResource;
use Illuminate\Http\Request;

class CitiesController extends Controller{

    public function index(){

    }

    /**
     * @param Request $request
     * @return CitiesResource|\Illuminate\Http\JsonResponse
     */
    public function filter(Request $request){
        $cityInfo = new CoordinatesHelper();
        $cities = $cityInfo->getCity($request);

        if ($cities)
            return new CitiesResource(collect($cities));
        else
            return response()->json(["data" =>['error_message' => 'Something went wrong']],500);
    }
}
