<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;

class CitiesResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Collection
     */
    public function toArray($request)
    {
        $cities = $this->collection->transform(function ($city){
           return [
               'name' => $city['name'],
               'longitude' => $city['latitude'],
               'latitude' => $city['longitude'],
               'info' => [
                   'clouds' => $city['info']['clouds'],
                   'dewPoint' => $city['info']['dewPoint'],
                   'temperature' => $city['info']['temperature'],
                   'windSpeed' => $city['info']['windSpeed'],
               ]
           ];
        });

        return $cities;
    }
}
