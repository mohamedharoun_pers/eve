<?php namespace App\Helpers;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class CoordinatesHelper
{

    private static $responseStyle = 'short'; // the length of the response
    private static $citySize = 'cities15000'; // the minimal number of citizens a city must have
    private static $radius = 30; // the radius in KM
    private static $maxRows = 4; // the maximum number of rows to retrieve
    private $api_url; // used external Api to get cities
    private $api_username; // username used for external Api

    /**
     * CoordinatesHelper constructor.
     */
    public function __construct ()
    {
        $this->api_username = config('auth.api_username');
        $this->api_url = config('auth.api_url');
    }


    /**
     * @param $latitude
     * @param $longitude
     * @return mixed
     */
    public function getCityInfo($latitude, $longitude){
        $client = new Client(['base_uri' => $this->api_url]);
        $city = json_decode($client->request('GET','findNearByWeatherJSON?lat='.$latitude.'&lng='.$longitude.'&username='.$this->api_username)->getBody()->getContents());

        return $city;
    }

    /**
     * @param $request
     * @return array|bool
     */
    public function getCity($request){
        try{
            if ($request->lnglat){
                $params = explode(',', $request->lnglat);
                if (count($params) == 2){
                    list($latitude, $longitude) = explode(',', $request->lnglat);
                    $getCitiesRequest  = $this->getCityByLatLong($latitude,$longitude);

                    return $this->getResponse($getCitiesRequest, $getCitiesRequest->getStatusCode());
                }
            }
            elseif ($request->name){
                $getCitiesRequest = $this->getCityByName($request->name);
                return $this->getResponse($getCitiesRequest, $getCitiesRequest->getStatusCode());
            }
        }
        catch (\Exception $exception){
            Log::error($exception->getMessage());
            return false;
        }
        return false;

    }

    /**
     * @param $data
     * @param $statusCode
     * @return array|bool
     */
    private function getResponse($data , $statusCode){
        if ($statusCode == 200){
            $cities = [];
            $cityObj = [];
            $data = json_decode($data->getBody()->getContents());
            foreach ($data->geonames as $city){
                $cityInfo = $this->getCityInfo($city->lat, $city->lng);
                $cities['name'] = $city->name;
                $cities['latitude'] = $city->lat;
                $cities['longitude'] = $city->lng;
                $cities['info']['clouds'] = $cityInfo->weatherObservation->clouds;
                $cities['info']['dewPoint'] = $cityInfo->weatherObservation->dewPoint;
                $cities['info']['temperature'] = $cityInfo->weatherObservation->temperature;
                $cities['info']['windSpeed'] = $cityInfo->weatherObservation->windSpeed;
                $cityObj[] = $cities;
            }
            return $cityObj;
        }
        else{
            return false;
        }
    }

    /**
     * @param $latitude
     * @param $longitude
     * @return \Psr\Http\Message\ResponseInterface
     */
    private function getCityByLatLong($latitude, $longitude){
        $client = new Client(['base_uri' => $this->api_url]);
        return $client->request('GET','findNearbyPlaceNameJSON?lat='.$latitude.'&lng='.$longitude.'&style='.self::$responseStyle.'&cities='.self::$citySize.'&radius='.self::$radius.'&maxRows='.self::$maxRows.'&username='.$this->api_username);
    }

    /**
     * @param $name
     * @return \Psr\Http\Message\ResponseInterface
     */
    private function getCityByName($name){
        $client = new Client(['base_uri' => $this->api_url]);
        $getCity = $client->request('GET','wikipediaSearchJSON?q='.$name.'&maxRows=1'.'&username='.$this->api_username);
        $cityInfo = collect(json_decode($getCity->getBody()->getContents())->geonames)->first();
        return $this->getCityByLatLong($cityInfo->lat , $cityInfo->lng);
    }
}

