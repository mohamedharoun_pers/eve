<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/cities', ['uses' => 'Api\Cities\CitiesController@index', 'as' => 'api.cities.all']);
Route::get('/cities/filter', ['uses' => 'Api\Cities\CitiesController@filter', 'as' => 'api.cities.filter']);
Route::fallback(function(){
    return response()->json([
        'data' => ['error_message' => 'Page Not Found']], 404);
});
